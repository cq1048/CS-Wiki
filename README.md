<br>
<br>

<p align="center">
    <img width="280px" src="https://gitee.com/veal98/images/raw/master/img/20210212163625.png" >
</p>


<div align="center">

[![star](https://gitee.com/veal98/CS-Wiki/badge/star.svg?theme=dark)](https://gitee.com/veal98/CS-Wiki/stargazers)
[![fork](https://gitee.com/veal98/CS-Wiki/badge/fork.svg?theme=dark)](https://gitee.com/veal98/CS-Wiki/members)
[![GitHub stars](https://img.shields.io/github/stars/Veal98/CS-Wiki?logo=github)](https://github.com/Veal98/CS-Wiki/stargazers)
[![GitHub forks](https://img.shields.io/github/forks/Veal98/CS-Wiki?logo=github)](https://github.com/Veal98/CS-Wiki/network)


<br>


|                          计算机基础                          |                          Java                           |                          必备框架                           |                          系统设计                           |                          工具                           | 前端                                                    |                          Python                           |                          机器学习                           |                          深度学习                           |                          强化学习                           |
| :----------------------------------------------------------: | :-----------------------------------------------------: | :---------------------------------------------------------: | :---------------------------------------------------------: | :-----------------------------------------------------: | ------------------------------------------------------- | :-------------------------------------------------------: | :---------------------------------------------------------: | :---------------------------------------------------------: | :---------------------------------------------------------: |
| [📑](https://veal98.gitee.io/cs-wiki/#/README?id=📑-计算机基础) | [🍵](https://veal98.gitee.io/cs-wiki/#/README?id=🍵-java) | [🔥](https://veal98.gitee.io/cs-wiki/#/README?id=🔥-必备框架) | [👷](https://veal98.gitee.io/cs-wiki/#/README?id=👷-系统设计) | [🔨](https://veal98.gitee.io/cs-wiki/#/README?id=🔨-工具) | [🎨](https://veal98.gitee.io/cs-wiki/#/README?id=🎨-前端) | [🐍](https://veal98.gitee.io/cs-wiki/#/README?id=🐍-python) | [🤖](https://veal98.gitee.io/cs-wiki/#/README?id=🤖-机器学习) | [🌺](https://veal98.gitee.io/cs-wiki/#/README?id=🌺-深度学习) | [🎮](https://veal98.gitee.io/cs-wiki/#/README?id=🎮-强化学习) |


</div>

---

💡 **「关于」**

- 🎓 博主东南大学研一在读，算法岗诸神黄昏已人尽皆知，遂决定深耕 Java，致力全栈。本仓库用于记录学习过程中的所思所想，并力图构建一个完善的知识体系，便于复习巩固

- 🙏 由于本人水平有限，仓库中的知识点来自本人原创、视频、书籍、博客等，非原创均已标明出处（或在参考资料中列出），如有遗漏或发现文章错误及排版问题，请提 issue 或 PR

- ⚡ **[Gitee 在线阅读（国内访问速度较快）](https://veal98.gitee.io/cs-wiki)** | [Gitee 仓库地址（推荐）](https://gitee.com/veal98/CS-Wiki)

- 🔮 [Github 在线阅读](https://veal98.github.io/CS-Wiki/) | [Github 仓库地址](https://github.com/Veal98/CS-Wiki)

## 📑 计算机基础

- [数据结构与算法](https://veal98.gitee.io/cs-wiki/#/README?id=数据结构与算法)
- [计算机网络](https://veal98.gitee.io/cs-wiki/#/README?id=计算机网络)
- [操作系统](https://veal98.gitee.io/cs-wiki/#/README?id=操作系统)
- [Linux](https://veal98.gitee.io/cs-wiki/#/README?id=linux)
- [数据库](https://veal98.gitee.io/cs-wiki/#/README?id=数据库)

## 🍵 Java

- [Java 8 基础](https://veal98.gitee.io/cs-wiki/#/README?id=java-8-%e5%9f%ba%e7%a1%80)
- [Java 虚拟机](https://veal98.gitee.io/cs-wiki/#/README?id=java-%e8%99%9a%e6%8b%9f%e6%9c%ba)
- [设计模式](https://veal98.gitee.io/cs-wiki/#/README?id=设计模式)

## 🔥 必备框架

- [SSM](https://veal98.gitee.io/cs-wiki/#/README?id=ssm)
- [Spring Boot 2.x](https://veal98.gitee.io/cs-wiki/#/README?id=spring-boot-2x)
- [Netty 4.x](https://veal98.gitee.io/cs-wiki/#/README?id=%e2%91%a2-netty-4x)

## 👷 系统设计

- [系统设计基础](https://veal98.gitee.io/cs-wiki/#/README?id=%e7%b3%bb%e7%bb%9f%e8%ae%be%e8%ae%a1%e5%9f%ba%e7%a1%80)
- [认证授权](https://veal98.gitee.io/cs-wiki/#/README?id=%e8%ae%a4%e8%af%81%e6%8e%88%e6%9d%83)
- [分布式](https://veal98.gitee.io/cs-wiki/#/README?id=%e5%88%86%e5%b8%83%e5%bc%8f)
- [高并发](https://veal98.gitee.io/cs-wiki/#/README?id=%e9%ab%98%e5%b9%b6%e5%8f%91)
- [高可用](https://veal98.gitee.io/cs-wiki/#/README?id=%e9%ab%98%e5%8f%af%e7%94%a8)
- [微服务](https://veal98.gitee.io/cs-wiki/#/README?id=%e5%be%ae%e6%9c%8d%e5%8a%a1)

## 🔨 工具

- [Git](https://veal98.gitee.io/cs-wiki/#/README?id=git)
- [Docker](https://veal98.gitee.io/cs-wiki/#/README?id=docker)

## 🎨 前端

- [必备基础知识](https://veal98.gitee.io/cs-wiki/#/README?id=%e5%bf%85%e5%a4%87%e5%9f%ba%e7%a1%80%e7%9f%a5%e8%af%86)
- [Vue 2.x](https://veal98.gitee.io/cs-wiki/#/README?id=vue-2x)

## 🐍 Python

- [Python 3 基础](https://veal98.gitee.io/cs-wiki/#/README?id=python-3-基础)
- [Python 数据分析](https://veal98.gitee.io/cs-wiki/#/README?id=python-数据分析)

## 🤖 机器学习

- [机器学习入门](https://veal98.gitee.io/cs-wiki/#/README?id=机器学习入门)
- [常见算法 Python 实现](https://veal98.gitee.io/cs-wiki/#/README?id=常见算法-python-实现)

## 🌺 深度学习

- [深度学习入门](https://veal98.gitee.io/cs-wiki/#/README?id=深度学习入门)
- [TensorFlow 2](https://veal98.gitee.io/cs-wiki/#/README?id=tensorflow-2)

## 🎮 强化学习

- [强化学习入门](https://veal98.gitee.io/cs-wiki/#/README?id=强化学习入门)
- [MATLAB](https://veal98.gitee.io/cs-wiki/#/README?id=matlab)

## 💬 公众号

我的公众号：**飞天小牛肉**，2020/12/29 日第一次开通，专注于分享计算机基础（数据结构 + 算法 + 计算机网络 + 数据库 + 操作系统 + Linux）和 Java 相关技术栈的相关原创技术好文。本公众号的目的就是**让大家可以快速掌握重点知识，有的放矢**。希望大家多多支持哦，和小牛肉一起成长

<img width="260px" src="https://gitee.com/veal98/images/raw/master/img/公众号二维码.png" >

## 📞 联系我

有什么问题也可以添加我的微信，记得备注来意：格式 <u>（学校或公司 - 姓名或昵称 - 来意）</u>

<img width="260px" src="https://gitee.com/veal98/images/raw/master/img/微信图片_20210105121328.jpg" >